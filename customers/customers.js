const express = require("express")
const app = express()
const mongoose = require("mongoose")
const bodyParser = require("body-parser")

app.use(bodyParser.json())
mongoose.connect("mongodb://localhost:27017/libraryCustomersDb")

require("./Customer")
const Customer = mongoose.model("Customer")

app.post("/customers", (req, res) => {
    var newCustomer = {
        name: req.body.name,
        age: req.body.age,
        address: req.body.address
    }

    var customer = Customer(newCustomer)

    customer.save().then(() => {
        res.send("Customer created successfully!")
    }).then(err => {
        if(err){
            throw err
        }
    })
})

app.get("/customers", (req, res) => {
    Customer.find().then((customers) => {
        res.json(customers)
    }).catch(err => {
        if(err){
            throw err
        }
    })
})

app.get("/customers/:id", (req, res) => {
    Customer.findById(req.params.id).then((customer) => {
        res.json(customer)
    }).catch(err => {
        if(err){
            throw err
        }
    })
})

app.delete("/customers/:id", (req, res) => {
    Customer.findByIdAndDelete(req.params.id).then(
        res.send("Customer deleted successfully")
    ).catch(err => {
        if(err){
            throw err
        }
    })
})

app.listen("5555", () => {
    console.log("Up and running - customer service!")
})