const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
mongoose.connect("mongodb://localhost:27017/libraryBooksDb")

app.use(bodyParser.json());

require("./Book")
const Book = mongoose.model("Book")

app.get("/", (req, res) => {
    res.send("Hey arnolds!")
})

app.post("/books", (req, res) => {
    console.log(req.body)
    var newBook = {
        title: req.body.title,
        author: req.body.author,
        numberPages: req.body.numberPages,
        publisher: req.body.publisher
    }
    var book = new Book(newBook)
    book.save().then(() => {
        console.log("New book created!")
    }).catch((err) => {
        if(err){
            throw err;
        }
    })
    res.send("A new book created successfully!")
})

app.get("/books", (req, res) => {
    Book.find().then((books) => {
        res.json(books)
    }).catch(err => {
        if(err){
            throw err
        }
    })
})

app.get("/books/:id", (req, res) => {
    Book.findById(req.params.id).then((book) => {
        if(book){
            res.json(book)
        }else{
            res.sendStatus(404)
        }
    }).catch(err => {
        if(err){
            throw err
        }
    })
})

app.delete("/books/:id", (req, res) => {
    Book.findByIdAndDelete(req.params.id).then(() => {
        res.send("Book removed successfully!")
    }).catch(err => {
        if(err){
            throw err
        }
    })
})

app.listen(4545, () => {
    console.log("Up and running! -- This is our Books service")
})