const express = require("express")
const app = express()
const bodyParser = require("body-parser");
const mongoose = require("mongoose")
const axios = require("axios")

mongoose.connect("mongodb://localhost:27017/libraryOrdersDb")
app.use(bodyParser.json());

require("./Order")

const Order = mongoose.model("Order")

app.post("/orders", (req, res) => {
    var newOrder = {
        CustomerID: new mongoose.Types.ObjectId(req.body.CustomerID),
        BookID: new mongoose.Types.ObjectId(req.body.BookID),
        initialDate: req.body.initialDate,
        deliveryDate: req.body.deliveryDate,
    }

    var order = new Order(newOrder)
    order.save().then(() => {
        res.send("Order created successfully")
    }).catch(err => {
        if(err){
            throw err
        }
    })
})

app.get("/orders", (req, res) => {
    Order.find().then(orders => {
        res.json(orders)
    }).catch(err => {
        if(err){
            throw err
        }
    })
})

app.get("/orders/:id", (req, res) => {
    Order.findById(req.params.id).then((order) => {
        if(order){
            axios.get("http://localhost:5555/customers/" + order.CustomerID).then(response => {
                var orderObject = {
                    customerName: response.data.name,
                    bookTitle: ''
                }

                axios.get("http://localhost:4545/books/"+order.BookID).then(response => {
                    console.log(response)
                    orderObject.bookTitle = response.data.title
                    res.json(orderObject)
                })
            })
        }else{
            res.send("Invalid order ID!")
        }
    }).catch(err => {
        if(err){
            throw err
        }
    })
})

app.listen(7777, () => {
    console.log("Up and running - Orders service")
})